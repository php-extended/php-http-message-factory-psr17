<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-message-factory-psr17 library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpMessage\ResponseFactory;
use PHPUnit\Framework\TestCase;

/**
 * ResponseFactoryTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpMessage\ResponseFactory
 *
 * @internal
 *
 * @small
 */
class ResponseFactoryTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ResponseFactory
	 */
	protected ResponseFactory $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testResponse() : void
	{
		$response = $this->_object->createResponse(200, 'OK');
		
		$this->assertEquals(200, $response->getStatusCode());
		$this->assertEquals('OK', $response->getReasonPhrase());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ResponseFactory();
	}
	
}
