<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-message-factory-psr17 library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpMessage;

use InvalidArgumentException;
use PhpExtended\Parser\ParseThrowable;
use PhpExtended\Uri\UriParser;
use PhpExtended\Uri\UriParserInterface;
use Psr\Http\Message\UriFactoryInterface;
use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * UriFactory class file.
 * 
 * This class creates uris based on the uri implementations of the
 * php-extended/php-http-message-psr7 library.
 * 
 * @author Anastaszor
 */
class UriFactory implements Stringable, UriFactoryInterface
{
	
	/**
	 * The parser of for the uris.
	 * 
	 * @var ?UriParserInterface
	 */
	protected static ?UriParserInterface $_parser = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\UriFactoryInterface::createUri()
	 */
	public function createUri(string $uri = '') : UriInterface
	{
		if(null === static::$_parser)
		{
			static::$_parser = new UriParser();
		}
		
		try
		{
			return static::$_parser->parse($uri);
		}
		catch(ParseThrowable $e)
		{
			throw new InvalidArgumentException($e->getMessage(), (int) $e->getCode(), $e);
		}
	}
	
}
