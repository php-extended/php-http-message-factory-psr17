<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-message-factory-psr17 library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpMessage;

use InvalidArgumentException;
use Psr\Http\Message\ServerRequestFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;
use Stringable;

/**
 * ServerRequestFactory class file.
 * 
 * This class creates server requests based on the request and uri
 * implementations of the php-extended/php-http-message-psr7 library.
 * 
 * @author Anastaszor
 */
class ServerRequestFactory implements ServerRequestFactoryInterface, Stringable
{
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Message\ServerRequestFactoryInterface::createServerRequest()
	 * @param UriInterface|string $uri
	 * @param array<array-key, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string|object>>> $serverParams
	 * @throws InvalidArgumentException
	 * @psalm-suppress MoreSpecificImplementedParamType
	 */
	public function createServerRequest(string $method, $uri, array $serverParams = []) : ServerRequestInterface
	{
		$srq = new ServerRequest();
		if(\is_string($uri))
		{
			$urifactory = new UriFactory();
			$uri = $urifactory->createUri($uri);
		}
		/** @var ServerRequestInterface $srq */
		$srq = $srq->withUri($uri);
		
		/** @var ServerRequestInterface $srq */ /** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
		$srq = $srq->withCookieParams($this->getSuperglobalParams('_COOKIE', $serverParams));
		/** @var ServerRequestInterface $srq */
		$srq = $srq->withQueryParams($this->getSuperglobalParams('_GET', $serverParams));
		/** @var ServerRequestInterface $srq */
		$srq = $srq->withParsedBody($this->getSuperglobalParams('_POST', $serverParams));
		
		$files = [];
		
		foreach($this->getSuperglobalParams('_FILES', $serverParams) as $key => $value)
		{
			/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
			$files += $this->collectFiles($key, $value);
		}
		/** @var ServerRequestInterface $srq */
		$srq = $srq->withUploadedFiles($files);
		
		foreach($serverParams as $name => $value)
		{
			/** @var ServerRequestInterface $srq */
			$srq = $srq->withAttribute($name, $value);
		}
		
		return $srq;
	}
	
	/**
	 * @param string $key
	 * @param array<string, array<string, string|array<string, string>>> $files
	 * @return array<string, UploadedFile>
	 */
	public function collectFiles(string $key, array $files) : array
	{
		if(!isset($files['name']) || !isset($files['error']) || !isset($files['type']) || !isset($files['tmp_name']) || !isset($files['size']))
		{
			return [];
		}
		
		/** @var string|array<int, string> $name */
		$name = $files['name'];
		$error = $files['error'];
		$types = $files['type'];
		$tmpns = $files['tmp_name'];
		$sizes = $files['size'];
		if(\is_string($name))
		{
			/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument,InvalidCast */
			return [$key => new UploadedFile($name, $tmpns, $types, $sizes, $error)];
		}
		
		$objects = [];
		
		foreach($name as $newkey => $nname)
		{
			/** @phpstan-ignore-next-line */ /** @psalm-suppress InvalidArgument */
			foreach($this->collectFiles($key.'['.((string) $newkey).']', [
				'name' => $nname,
				'error' => $files['error'][$key],
				'type' => $files['type'][$key],
				'tmp_name' => $files['tmp_name'][$key],
				'size' => $files['size'][$key],
			]) as $newkey2 => $value)
			{
				$objects[$newkey2] = $value;
			}
		}
		
		return $objects;
	}
	
	/**
	 * Gets the data from the given superglobal.
	 * 
	 * @param string $superName
	 * @param array<string, null|boolean|integer|float|string|array<string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string|object>>> $serverParams
	 * @return array<string, null|boolean|integer|float|string|array<string, null|boolean|integer|float|string|array<integer|string, null|boolean|integer|float|string|object>>>
	 * @psalm-suppress MixedInferredReturnType
	 */
	public function getSuperglobalParams(string $superName, array &$serverParams) : array
	{
		if(!isset(${$superName}))
		{
			return [];
		}
		
		$data = ${$superName};
		if(isset($serverParams[$superName]) && \is_array($serverParams[$superName]))
		{
			/** @psalm-suppress MixedArgument */
			$data = \array_merge($data, $serverParams[$superName]);
			unset($serverParams[$superName]);
		}
		
		/** @psalm-suppress MixedReturnStatement */
		return $data;
	}
	
}
